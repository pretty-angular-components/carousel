export const elements = [
  {name: 'Jenfirebreatha', img: 'Dragonborn-sorcerer-portrait.png'},
  {name: 'Brainsra', img: 'Dwarf-barbarian-portrait.png'},
  {name: 'Pacoffin', img: 'Dwarf-fighter-portrait.png'},
  {name: 'Quatorifiliny', img: 'Elf-ranger-portrait.png'},
  {name: 'Rosepop', img: 'Gnome-bard-portrait.png'},
  {name: 'Agamason', img: 'Gnome-wizard-portrait.png'},
  {name: 'Mooreconda', img: 'Half-elf-cleric-portrait.png'},
  {name: 'Hayetwinkle', img: 'Human-paladin-portrait.png'}
];
