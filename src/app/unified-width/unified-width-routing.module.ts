import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UnifiedWidthComponent} from './unified-width.component';

const routes: Routes = [
  { path: '', component: UnifiedWidthComponent}
];

@Injectable()
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnifiedWidthRoutingModule { }
