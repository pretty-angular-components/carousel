import { Component } from '@angular/core';
import {elements} from '../elements';

@Component({
  selector: 'app-bootstrap',
  templateUrl: './bootstrap.component.html',
  styleUrls: ['./bootstrap.component.scss']
})
export class BootstrapComponent  {

  elements = elements;

  constructor() { }

  sayHi(elem: {name: string, img: string}) {
    console.log(`Hi ${elem.name}!`);
  }

}
