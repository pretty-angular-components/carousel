import {ElementRef, EmbeddedViewRef, ViewContainerRef} from '@angular/core';
import {CarouselItemDirective} from './directives/carousel-item.directive';

export class Item {
  view: EmbeddedViewRef<object>;
  rect: any;
  private marginLeft = 0;
  private marginRight = 0;
  private defaultWidth = 0;

  private nextItem: Item = null;
  private prevItem: Item = null;

  constructor(public template: CarouselItemDirective,
              public index: number,
              public carousel: ElementRef,
              public itemsContainer: ViewContainerRef,
              public perPage: boolean | number = false,
              positionInQue?: number) {
    this.view = this.itemsContainer
      .createEmbeddedView(template.templateRef, null, positionInQue) as EmbeddedViewRef<object>;
  }

  saveInitialState() {
    const computedStyles = window.getComputedStyle(this.rootNode, null);
    this.marginLeft = parseFloat(computedStyles.marginLeft);
    this.marginRight = parseFloat(computedStyles.marginRight);
    this.defaultWidth = parseFloat(computedStyles.width);
  }

  update() {
    if (!this.view.rootNodes.length) {
      return;
    }

    const computedStyles = window.getComputedStyle(this.rootNode, null);
    this.marginLeft = parseFloat(computedStyles.marginLeft);
    this.marginRight = parseFloat(computedStyles.marginRight);

    if (this.perPage === false) {
      this.rootNode.style.width = this.defaultWidth + 'px';
    }

    if (this.perPage || this.template.fullSize) {
      const carouselWidth = this.carousel.nativeElement.getBoundingClientRect().width;
      if (this.template.fullSize) {
        this.rootNode.style.width = (carouselWidth - this.margin) + 'px';
      }
      if (this.perPage) {
        this.rootNode.style.width = (carouselWidth / Number(this.perPage) - this.margin) + 'px';
      }
    }

    this.rect = this.rootNode.getBoundingClientRect();
  }

  destroy() {
    this.view.destroy();
  }

  get margin() {
    return this.marginLeft + this.marginRight;
  }

  get width() {
    return this.rect.width + this.margin;
  }

  get left() {
    return this.rect.left - this.marginLeft;
  }

  get right() {
    return this.rect.right + this.marginRight;
  }

  set next(val: Item) {
    this.nextItem = val;
  }

  set prev(val: Item) {
    if (this.prevItem) { this.prevItem.next = null; }
    this.prevItem = val;
    if (this.prevItem) { this.prevItem.next = this; }
  }

  get offsetLtR(): number {
    return this.prevItem ? (this.prevItem.width + this.prevItem.offsetLtR) : 0;
  }

  get offsetRtL(): number {
    return this.width + (this.nextItem ? this.nextItem.offsetRtL : 0);
  }

  private get rootNode(): any {
    return this.view.rootNodes[0];
  }
}
