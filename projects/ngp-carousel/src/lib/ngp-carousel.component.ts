import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style} from '@angular/animations';
import {CarouselItemDirective} from './directives/carousel-item.directive';
import {AnimateConfig} from './animate/AnimateConfig';
import {enControlsMode, enDirection, enSlot} from './const';
import {Observable, Subscription, timer} from 'rxjs';
import {CarouselButtonDirective} from './directives/carousel-button.directive';
import {CarouselButtonComponent} from './carousel-button/carousel-button.component';
import {WidthChangedEvent} from './directives/width-changed-event';
import {Slot} from './slot';
import {Item} from './item';

@Component({
  selector: 'ngp-carousel',
  templateUrl: './ngp-carousel.component.html',
  styleUrls: ['./ngp-carousel.component.scss']
})
export class NgpCarouselComponent implements AfterViewInit, OnDestroy {

  @ViewChild('buttonSlotLeft', { read: ViewContainerRef }) set buttonSlotLeft(slot: ViewContainerRef) {
    this.slots[enSlot.left].slotRef = slot;
  }
  @ViewChild('buttonSlotRight', { read: ViewContainerRef }) set buttonSlotRight(slot: ViewContainerRef) {
    this.slots[enSlot.right].slotRef = slot;
  }

  @ViewChild('container', { read: ViewContainerRef }) private containerViewRef: ViewContainerRef;
  @ViewChild('carousel') private carousel: ElementRef;
  @ContentChildren(CarouselItemDirective) itemTemplates: QueryList<CarouselItemDirective>;
  @ViewChild('items', { read: ViewContainerRef }) private itemsContainerRef: ViewContainerRef;

  @ContentChildren(CarouselButtonDirective) outerButtons: QueryList<CarouselButtonDirective>;

  @Input() rtl = false;
  @Input() set loop(val: boolean) {
    this.$loop = val;
    if (this.$loop) {
      this.runSlideTimer();
    }
  }

  @Input() set disabled(val: boolean) {
    this.$disabled = val;
    if (!this.$disabled) {
      this.runSlideTimer();
    }
  }

  @Input() set perPage(val: number | boolean) {
    this.$perPage = val;
    if (this.items?.length) {
      this.items?.forEach(item => {
        item.perPage = this.perPage;
        item.update();
      });
      this.instantlyMoveTo(this.cursor);
      this.checkIsButtonsDisabled();
    }
  }

  @Input() buttonsMode: enControlsMode = enControlsMode.show;
  @Input() innerButtons = false;

  @Output() animStart: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() animFinish: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() set autoSlide(val: number | boolean)  {
    this.autoSlideTime = val;
    if (this.autoSlideTimerSubscription) {
      this.autoSlideTimerSubscription.unsubscribe();
      this.autoSlideTimerSubscription = null;
    }
  }

  @Input() autoSlideDirection: enDirection | string = enDirection.next;
  @Input() set animationDelay(value: number) { this.animationConfig.delay = value; }
  @Input() set animationDuration(value: number) { this.animationConfig.duration = value; }
  @Input() set animationEasing(value: string) { this.animationConfig.easing = value; }

  get autoSlide(): boolean | number {
    return this.autoSlideTime;
  }

  get disabled(): boolean {
    return this.$disabled;
  }

  get loop(): boolean {
    return this.$loop;
  }

  get perPage(): boolean | number {
    return this.$perPage;
  }

  private get itemsWidth(): number {
    return Math.trunc((!this.items || !this.items.length) ? 0 : Math.abs(this.rtl ?
      (this.items[this.items.length - 1].left - this.items[0].right) :
      (this.items[this.items.length - 1].right - this.items[0].left)
    ));
  }

  private $disabled = false;
  private $loop = false;
  private $perPage: number | boolean = false;
  private carouselWidth: number;
  private $cursor = 0;
  private currentOffset = 0;
  private isEndByOffsetReached = false;
  private autoSlideTime: number | boolean;
  private autoSlideTimerSubscription: Subscription = null;
  private animationConfig = new AnimateConfig();

  private slots: Slot[] = [];
  private items: Item[] = [];

  showButtons = false;
  private player: AnimationPlayer = null;
  private isScrolling = false;

  /**
   * this variable required for case when component destroyed but animation is active.
   * preventing rerun loop animation after component was destroyed
   */
  private forceStop = false;

  get isOnStart() { return this.cursor === 0; }
  get isOnEnd() { return this.isEndByOffsetReached || (!this.loop && this.cursor === this.items.length - 1); }
  get issetPlayer() { return this.player !== null; }

  /** returned index of template that cursor placed on */
  get index(): number {
    return this.items.length ? this.items[this.$cursor].index : 0;
  }

  private set cursor(val: number) {
    this.$cursor = val;
  }

  private get cursor(): number {
    return this.$cursor;
  }

  private get lastReachableCursor(): number {
    if (!this.itemTemplates.length) {
      return 0;
    }

    let lastCursor = this.itemTemplates.length - 1;
    let shift = 0;
    const maxShift = this.itemsWidth - this.carouselWidth;
    for (let i = 0; i < this.itemTemplates.length; i++) {
      if (shift >= maxShift) {
        lastCursor = i;
        break;
      }
      shift += this.items[i].width;
    }
    return lastCursor;
  }

  private get nextButton(): Slot {
    return this.rtl ? this.slots[enSlot.left] : this.slots[enSlot.right];
  }

  private get prevButton(): Slot {
    return this.rtl ? this.slots[enSlot.right] : this.slots[enSlot.left];
  }

  constructor(private builder: AnimationBuilder,
              private cd: ChangeDetectorRef,
              private render: Renderer2,
              private el: ElementRef,
              private resolver: ComponentFactoryResolver) {

    this.slots[enSlot.left] = new Slot(enSlot.left);
    this.slots[enSlot.right] = new Slot(enSlot.right);
  }

  ngAfterViewInit(): void {
    this.slots[enSlot.left].outerButton = this.outerButtons.find(button => button.slot === enSlot.left);
    this.slots[enSlot.right].outerButton = this.outerButtons.find(button => button.slot === enSlot.right);

    this.init();

    /** reset animation to initial state, I haven't found next way to reset state without animation
     * that's why I have used this logic.
     * besides, reinit carousel after number of items changes
     */
    this.itemTemplates.changes.subscribe(() => this.animate(0).subscribe(() => this.init()));
  }

  ngOnDestroy() {
    this.forceStop = true;
    this.autoSlideTimerSubscription?.unsubscribe();
    this.player?.finish();
    this.player?.destroy();
  }

  fill() {
    this.itemTemplates.forEach((template, index) => {
      const item = new Item(
        template,
        index,
        this.carousel,
        this.itemsContainerRef,
        this.perPage,
        this.rtl ? 0 : undefined
      );
      item.prev = this.items.length > 0 ? this.items[this.items.length - 1] : null;
      this.items.push(item);
    });

    /** this detectChanges required for correct detect sizes of new items */
    this.cd.detectChanges();
    this.items.forEach(item => {
        item.saveInitialState();
        item.update();
      }
    );

    this.checkIsButtonsDisabled();
  }

  prev() {
    if (this.stopControlAction(this.isOnStart)) { return; }

    this.cursor--;
    this.animate(this.countAnimationOffset()).subscribe(() => {
      if (this.loop && this.cursor <= 0) {
        this.instantlyMoveTo(this.itemTemplates.length);
      }
    });
  }

  next() {
    if (this.stopControlAction(this.isOnEnd)) { return; }

    this.cursor++;
    this.animate(this.countAnimationOffset()).subscribe(() => {
      if (this.loop && this.cursor >= this.itemTemplates.length * 2) {
        this.instantlyMoveTo(this.itemTemplates.length);
      }
    });
  }

  toStart(instantly?: boolean) {
    if (this.loop) { return; }

    this.cursor = 0;
    this.animate(this.countAnimationOffset(), instantly).subscribe();
  }

  toEnd(instantly?: boolean) {
    if (this.loop) { return; }

    this.cursor = this.lastReachableCursor;
    this.animate(this.countAnimationOffset(), instantly).subscribe();
  }

  move(dir: enDirection) {
    if (!this.items?.length) { return; }

    dir === enDirection.prev ? this.prev() : this.next();
  }

  widthChanged(event: WidthChangedEvent) {
    if (event.width) {
      this.carouselWidth = Math.trunc(event.width);
      this.items.forEach(item => item.update());
      /** resetting value of cursorEnd. Required for correct processing buttons blocking
       * when windows size changes on when carouses width was greater than width of items
       */
      this.animate(this.countAnimationOffset(), true).subscribe();
    }
  }

  clear() {
    this.items?.forEach(item => item.destroy());
    this.items = [];

    this.checkIsButtonsDisabled();
  }

  reset() {
    this.items?.forEach(item => item.update());
    this.checkButtonsVisible();
    this.checkIsButtonsDisabled();
  }

  private init() {
    this.clear();

    /** fill base amount of items and count sizes */
    this.fill();

    /** fill additional items for loop mode if it's necessary and recount items */
    this.configureLoopMode();
    this.createAndConfigureControls();
    this.cd.detectChanges();

    this.carouselWidth = Math.trunc(this.carousel.nativeElement.offsetWidth);
    this.toStart(true);

    this.runSlideTimer();
  }

  private configureLoopMode() {
    if (!this.loop || !this.items?.length) { return; }

    const copyLoop = Math.ceil(this.carouselWidth / this.itemsWidth) + 1;
    for (let i = 0; i < copyLoop; i++) {
      this.fill();
    }

    this.instantlyMoveTo(this.itemTemplates.length);
  }

  private createAndConfigureControls() {
    this.configureButtons();
    this.checkButtonsVisible();
    this.checkIsButtonsDisabled();
  }

  private configureButtons() {
    this.configureButton(this.prevButton, () => this.prev());
    this.configureButton(this.nextButton, () => this.next());
  }

  private configureButton(slot: Slot, callback: any) {
    slot.validate(this.resolver.resolveComponentFactory(CarouselButtonComponent));
    if (!slot.isDisableDefaultClick) {
      this.render.listen(slot.nativeElementRef, 'click', callback);
    }
  }

  private checkButtonsVisible() {
    switch (this.buttonsMode) {
      case enControlsMode.show: { this.showButtons = true; } break;
      case enControlsMode.hide: { this.showButtons = false; } break;
      default: { this.showButtons = this.itemsWidth > this.carouselWidth; } break;
    }
  }

  private checkIsButtonsDisabled() {
    this.prevButton.disabled = (this.isOnStart && !this.loop) || this.itemsWidth <= this.carouselWidth || this.disabled;
    this.nextButton.disabled = (this.isOnEnd && !this.loop) || this.itemsWidth <= this.carouselWidth || this.disabled;
  }

  private runSlideTimer() {
    if (this.disabled || !this.autoSlideTime || !this.loop || this.forceStop) { return; }

    if (this.autoSlideTimerSubscription) {
      this.autoSlideTimerSubscription.unsubscribe();
      this.autoSlideTimerSubscription = null;
    }

    this.autoSlideTimerSubscription = timer(this.autoSlideTime as number).subscribe(
      () => this.move(this.autoSlideDirection as enDirection)
    );
  }

  private stopControlAction(startOrEnd: boolean) {
    return this.$disabled || this.issetPlayer || this.isScrolling || (startOrEnd && !this.loop);
  }

  private countAnimationOffset(): number {
    if (!this.items?.length) {
      return 0;
    }
    /** need to shift items on the opposite direction to how they placement */
    return -(this.rtl ? this.countOffsetRtL() : this.countOffsetLtR());
  }

  private countOffsetLtR(): number {
    const maxOffset = Math.trunc(this.itemsWidth - this.carouselWidth);
    let offset = Math.trunc(this.items[this.cursor].offsetLtR);
    offset = this.validateOffset(offset >= maxOffset, offset, maxOffset);

    return offset > 0 ? offset : 0;
  }

  private countOffsetRtL(): number {
    const maxOffset = 0;
    let offset = Math.trunc(this.items[this.cursor].offsetRtL - this.carouselWidth);
    const minOffset = Math.trunc(this.carouselWidth - this.itemsWidth);
    offset = this.validateOffset(offset <= maxOffset, offset, maxOffset);

    return offset < minOffset ? -minOffset : offset;
  }

  private validateOffset(condition: boolean, offset: number, maxOffset: number) {
    if (!this.loop) {
      this.isEndByOffsetReached = false;
      if (condition) {
        offset = maxOffset;
        /** when maxOffset les then 0 it's mean width of carousel greater than items length and end cant be reached */
        if (offset >= 0 || this.cursor === this.items.length - 1) {
          this.isEndByOffsetReached = true;
        }
      }
    }
    return offset;
  }

  private animate(offset: number, instantly?: boolean) {
    this.checkIsButtonsDisabled();
    this.checkButtonsVisible();

    return new Observable((resolver) => {
      if (!this.carousel || this.currentOffset === offset) {
        resolver.next(false);
        resolver.complete();
        return;
      }

      this.currentOffset = offset;
      if (!instantly) {
        this.animStart.emit(true);
      }

      const animation: AnimationFactory = this.builder.build([
        animate(
          instantly ? 0 : this.animationConfig.timing,
          style({ transform: `translateX(${offset}px)` })
        )
      ]);

      if (this.player) {
        this.player.destroy();
        this.player = null;
      }

      this.player = animation.create(this.carousel.nativeElement);
      this.player.play();
      this.player.onDone(() => {
        this.isScrolling = false;
        this.player = null;

        if (!instantly) {
          this.runSlideTimer();
          this.animFinish.emit(true);
        }

        resolver.next(true);
        resolver.complete();
      });
    });
  }

  private instantlyMoveTo(cursorPosition: number) {
    this.cursor = cursorPosition;
    this.animate(this.countAnimationOffset(), true).subscribe();
  }
}
