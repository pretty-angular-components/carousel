import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgpCarouselComponent } from './ngp-carousel.component';
import { CarouselItemDirective } from './directives/carousel-item.directive';
import { CarouselButtonComponent } from './carousel-button/carousel-button.component';
import { CarouselButtonDirective } from './directives/carousel-button.directive';
import { WidthChangedDirective } from './directives/width-changed.directive';

@NgModule({
  declarations: [
    NgpCarouselComponent,
    CarouselButtonComponent,
    CarouselItemDirective,
    CarouselButtonDirective,
    WidthChangedDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    WidthChangedDirective,
    NgpCarouselComponent,
    CarouselItemDirective,
    CarouselButtonComponent,
    CarouselButtonDirective
  ],
  entryComponents: [
    CarouselButtonComponent
  ],
})
export class NgpCarouselModule { }
